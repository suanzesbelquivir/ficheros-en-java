import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
//Prueba de BitBucket
public class Principal {

	private static final String nombrefichero = "prueba.txt";

	public static void main(String[] args) {

		try {
			FileWriter writer = new FileWriter(nombrefichero);
			BufferedWriter bufWriter = new BufferedWriter(writer);

			Scanner teclado = new Scanner(System.in);
			String texto;
			do {
				System.out.println("Introduce texto:");
				texto = teclado.nextLine();

				bufWriter.write(texto + "\n");

			} while (!texto.equals("."));

			bufWriter.flush();
			bufWriter.close();
			writer.close();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		FileReader reader = null;
		BufferedReader bufReader = null;

		try {
			reader = new FileReader(nombrefichero);
			bufReader = new BufferedReader(reader);

			String linea;
			while ((linea = bufReader.readLine()) != null) {
				System.out.println(linea);
			}

			bufReader.close();
			reader.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	} //Prueba de bit
}
